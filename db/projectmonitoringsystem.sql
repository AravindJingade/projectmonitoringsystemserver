CREATE SCHEMA IF NOT EXISTS "superior";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE superior."department" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"deptName" varchar(50) NOT NULL,
	"hod" varchar(50) NOT NULL,
	"coOrdinator" varchar(50)NOT NULL,
	"createdOn" timestamptz NULL,
	"updatedOn" timestamptz NULL,
	"otherinfo" varchar(50) NOT NULL
);
CREATE TABLE superior."users" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"username" varchar(50) NOT NULL,
	"password" varchar(50) NOT NULL,
	"isActive" varchar(50) NOT NULL,
	"status" varchar(50) NOT NULL,
	"isAdmin" varchar(50) NOT NULL,
	"createdOn" timestamptz NULL,
	"updatedOn" timestamptz NULL,
	"otherinfo" varchar(50) NOT NULL
);
CREATE TABLE superior."faculty" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"firstName" varchar(50) NOT NULL,
	"lastName" varchar(50) NOT NULL,
	"deptId" integer NOT NULL,
	"qualifiaction" varchar(50) NOT NULL,
	"emailID" varchar(50) NOT NULL,
	"createdOn" timestamptz NULL,
	"updatedOn" timestamptz NULL,
	"otherinfo" varchar(50) NOT NULL,
	CONSTRAINT "FK_department_faculty" FOREIGN KEY ( "deptId" ) REFERENCES "superior"."department" ( "id" )
);

CREATE TABLE superior."student" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"usn" varchar(50) NOT NULL,
	"firstName" varchar(50) NOT NULL,
	"lastName" varchar(50) NOT NULL,
	"deptId" integer NOT NULL,
	"sem" integer NOT NULL,
	"emailId" varchar(50) NOT NULL,
	"contactNo" varchar(12) NOT NULL,
	"otherInfo" varchar(50) NULL,
	"createdOn" timestamptz NULL,
	"updateOn" timestamptz NULL,
);

CREATE TABLE superior."statusTable" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"shortName" varchar(50) NOT NULL,
	"name" varchar(50) NOT NULL
);

CREATE TABLE superior."synopsisCall" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"preferredTopic" varchar(50) NOT NULL,
	"noofSynopsis" integer NOT NULL,
	"dueDate" varchar(50) NOT NULL,
	"createdOn" timestamptz NULL,
	"updateOn" timestamptz NULL,
	"otherInfo" varchar(50) NULL,
	"status" integer,
	"reportType" integer,
	"groupId"  integer NOT NULL,
	"pathUrl" varchar(100) NOT NULL,
	CONSTRAINT "FK_synopsisCall_group" FOREIGN KEY ( "groupId" ) REFERENCES "superior"."group" ( "id" )
);

CREATE TABLE superior."group" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"groupName" varchar(50) NOT NULL,
	"facultyId" integer NOT NULL,
	"createdOn" timestamptz NULL,
	"updatedOn" timestamptz NULL,
	"otherinfo" varchar(50) NOT NULL,
	CONSTRAINT "FK_department_faculty" FOREIGN KEY ( "facultyId" ) REFERENCES "superior"."faculty" ( "id" )
);


CREATE TABLE superior."studentGroup" (
	"id"  integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ) PRIMARY KEY,
	"studentId" integer NOT NULL,
	"groupId" integer NOT NULL,
	"createdOn" timestamptz NULL,
	"updateOn" timestamptz NULL,
	CONSTRAINT "FK_group_student" FOREIGN KEY ( "groupId" ) REFERENCES "superior"."group" ( "id" )
	CONSTRAINT "FK_student_group" FOREIGN KEY ( "studentId" ) REFERENCES "superior"."student" ( "id" )

);

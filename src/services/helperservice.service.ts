import {bind} from '@loopback/core';
import {repository} from '@loopback/repository';
import {FacultyRepository} from '../repositories';

@bind({tags: {namespace: 'services', name: 'helperserviceService'}})
export class HelperserviceService {
  constructor(
    @repository(FacultyRepository) public facultyRepository: FacultyRepository,
  ) {}

  async findFaulty(datas: any) {
    let data = await this.facultyRepository.find({
      include: [{relation: 'dept'}],
    });
    // console.log('data', data);
    return data;
  }
}

import {Entity, model, property, belongsTo, hasMany} from '@loopback/repository';
import {Department} from './department.model';
import {Group} from './group.model';

@model({
  settings: {idInjection: false, postgresql: {schema: 'superior', table: 'faculty'}}
})
export class Faculty extends Entity {
  @property({
    type: 'number',
    scale: 0,
    id: true,
    postgresql: {columnName: 'id', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'firstName', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'lastName', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  lastName: string;
  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'qualifiaction', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  qualifiaction: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'emailID', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  emailId: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'createdOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdOn?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'updatedOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedOn?: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'otherinfo', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  otherinfo: string;

  @belongsTo(() => Department)
  deptId: number;

  @hasMany(() => Group)
  groups: Group[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Faculty>) {
    super(data);
  }
}

export interface FacultyRelations {
  // describe navigational properties here
}

export type FacultyWithRelations = Faculty & FacultyRelations;

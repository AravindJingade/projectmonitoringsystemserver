import {Entity, model, property, hasMany} from '@loopback/repository';
import {Faculty} from './faculty.model';

@model({
  settings: {idInjection: false, postgresql: {schema: 'superior', table: 'department'}}
})
export class Department extends Entity {
  @property({
    type: 'number',
    scale: 0,
    id: true,
    postgresql: {columnName: 'id', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'deptName', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  deptName: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'hod', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  hod: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'coOrdinator', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  coOrdinator: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'createdOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdOn?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'updatedOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedOn?: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'otherinfo', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  otherinfo: string;

  @hasMany(() => Faculty, {keyTo: 'deptId'})
  faculties: Faculty[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Department>) {
    super(data);
  }
}

export interface DepartmentRelations {
  // describe navigational properties here
}

export type DepartmentWithRelations = Department & DepartmentRelations;

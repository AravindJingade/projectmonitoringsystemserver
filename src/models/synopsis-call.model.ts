import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Group} from './group.model';
import {StatusTable} from './status-table.model';

@model({
  settings: {idInjection: false, postgresql: {schema: 'superior', table: 'synopsisCall'}}
})
export class SynopsisCall extends Entity {
  @property({
    type: 'number',
    scale: 0,
    id: true,
    postgresql: {columnName: 'id', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'preferredTopic', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  preferredTopic: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'noofSynopsis', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  noofSynopsis: number;

  @property({
    type: 'string',
    required: true,
    scale: 0,
    postgresql: {columnName: 'dueDate', dataType: 'string', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  dueDate: number;

  @property({
    type: 'date',
    postgresql: {columnName: 'createdOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdOn?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'updateOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updateOn?: string;

  @property({
    type: 'string',
    length: 50,
    postgresql: {columnName: 'otherInfo', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  otherInfo?: string;

  @property({
    type: 'string',
    length: 100,
    postgresql: {columnName: 'pathUrl', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  pathUrl?: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'reportType', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  reportType: number;

  @belongsTo(() => Group)
  groupId: number;

  @belongsTo(() => StatusTable, {name: 'statusInfo'})
  status: number;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<SynopsisCall>) {
    super(data);
  }
}

export interface SynopsisCallRelations {
  // describe navigational properties here
}

export type SynopsisCallWithRelations = SynopsisCall & SynopsisCallRelations;

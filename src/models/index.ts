export * from './users.model';
export * from './department.model';
export * from './faculty.model';
export * from './student.model';
export * from './status-table.model';
export * from './synopsis-call.model';
export * from './group.model';
export * from './student-group.model';

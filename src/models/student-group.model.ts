import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Group} from './group.model';
import {Student} from './student.model';

@model({
  settings: {idInjection: false, postgresql: {schema: 'superior', table: 'studentGroup'}}
})
export class StudentGroup extends Entity {
  @property({
    type: 'number',
    required: false,
    scale: 0,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  id: number;
  @property({
    type: 'date',
    postgresql: {columnName: 'createdOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdOn?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'updateOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updateOn?: string;

  @belongsTo(() => Student)
  studentId: number;

  @belongsTo(() => Group)
  groupId: number;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StudentGroup>) {
    super(data);
  }
}

export interface StudentGroupRelations {
  // describe navigational properties here
}

export type StudentGroupWithRelations = StudentGroup & StudentGroupRelations;

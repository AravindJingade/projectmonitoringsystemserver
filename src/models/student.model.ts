import {Entity, model, property, hasMany} from '@loopback/repository';
import {StudentGroup} from './student-group.model';

@model({
  settings: {idInjection: false, postgresql: {schema: 'superior', table: 'student'}}
})
export class Student extends Entity {
  @property({
    type: 'number',
    scale: 0,
    id: true,
    postgresql: {columnName: 'id', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'usn', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  usn: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'firstName', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'lastName', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  lastName: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'deptId', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  deptId: number;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'sem', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  sem: number;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'emailId', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  emailId: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'contactNo', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  contactNo: number;

  @property({
    type: 'string',
    length: 50,
    postgresql: {columnName: 'otherInfo', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  otherInfo?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'createdOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdOn?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'updateOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updateOn?: string;

  @hasMany(() => StudentGroup, {keyTo: 'id'})
  studentGroups: StudentGroup[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Student>) {
    super(data);
  }
}

export interface StudentRelations {
  // describe navigational properties here
}

export type StudentWithRelations = Student & StudentRelations;

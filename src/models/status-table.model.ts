import {Entity, model, property, hasMany} from '@loopback/repository';
import {SynopsisCall} from './synopsis-call.model';

@model({
  settings: {idInjection: false, postgresql: {schema: 'superior', table: 'statusTable'}}
})
export class StatusTable extends Entity {
  @property({
    type: 'number',
    scale: 0,
    id: true,
    postgresql: {columnName: 'id', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'shortName', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  shortName: string;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'name', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  name: string;

  @hasMany(() => SynopsisCall, {keyTo: 'id'})
  synopsisCalls: SynopsisCall[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StatusTable>) {
    super(data);
  }
}

export interface StatusTableRelations {
  // describe navigational properties here
}

export type StatusTableWithRelations = StatusTable & StatusTableRelations;

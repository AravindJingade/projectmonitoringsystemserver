import {belongsTo, Entity, hasMany, model, property} from '@loopback/repository';
import {Faculty} from './faculty.model';
import {StudentGroup} from './student-group.model';
import {SynopsisCall} from './synopsis-call.model';

@model({
  settings: {idInjection: false, postgresql: {schema: 'superior', table: 'group'}}
})
export class Group extends Entity {
  @property({
    type: 'number',
    scale: 0,
    id: true,
    postgresql: {columnName: 'id', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    length: 50,
    postgresql: {columnName: 'groupName', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  groupName: string;
  @property({
    type: 'date',
    postgresql: {columnName: 'createdOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdOn?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'updatedOn', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedOn?: string;

  @property({
    type: 'string',
    required: false,
    length: 50,
    postgresql: {columnName: 'otherinfo', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  otherinfo: string;

  @property({
    type: 'string',
    required: false,
    length: 50,
    postgresql: {columnName: 'password', dataType: 'character varying', dataLength: 50, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  password: string;

  @belongsTo(() => Faculty)
  facultyId: number;

  @hasMany(() => StudentGroup, {keyTo: 'id'})
  studentGroups: StudentGroup[];

  @hasMany(() => SynopsisCall, {keyTo: 'id'})
  synopsisCalls: SynopsisCall[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Group>) {
    super(data);
  }
}

export interface GroupRelations {
  // describe navigational properties here
}

export type GroupWithRelations = Group & GroupRelations;

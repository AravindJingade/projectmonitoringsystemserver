export * from './department.repository';
export * from './faculty.repository';
export * from './status-table.repository';
export * from './student.repository';
export * from './synopsis-call.repository';
export * from './users.repository';
export * from './group.repository';
export * from './student-group.repository';

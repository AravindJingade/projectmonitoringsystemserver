import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasManyRepositoryFactory, repository, BelongsToAccessor} from '@loopback/repository';
import {PmssDataSource} from '../datasources';
import {Group, SynopsisCall, SynopsisCallRelations, StatusTable} from '../models';
import {GroupRepository} from './group.repository';
import {StatusTableRepository} from './status-table.repository';

export class SynopsisCallRepository extends DefaultCrudRepository<
  SynopsisCall,
  typeof SynopsisCall.prototype.id,
  SynopsisCallRelations
> {

  public readonly groups: HasManyRepositoryFactory<Group, typeof SynopsisCall.prototype.id>;

  public readonly group: BelongsToAccessor<Group, typeof SynopsisCall.prototype.id>;

  public readonly statusInfo: BelongsToAccessor<StatusTable, typeof SynopsisCall.prototype.id>;

  constructor(@inject('datasources.db') dataSource: PmssDataSource, @repository.getter('GroupRepository') protected groupRepositoryGetter: Getter<GroupRepository>, @repository.getter('StatusTableRepository') protected statusTableRepositoryGetter: Getter<StatusTableRepository>,) {
    super(SynopsisCall, dataSource);
    this.statusInfo = this.createBelongsToAccessorFor('statusInfo', statusTableRepositoryGetter,);
    this.registerInclusionResolver('statusInfo', this.statusInfo.inclusionResolver);
    this.group = this.createBelongsToAccessorFor('group', groupRepositoryGetter,);
    this.registerInclusionResolver('group', this.group.inclusionResolver);
  }
}

import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {PmssDataSource} from '../datasources';
import {StatusTable, StatusTableRelations, SynopsisCall} from '../models';
import {SynopsisCallRepository} from './synopsis-call.repository';

export class StatusTableRepository extends DefaultCrudRepository<
  StatusTable,
  typeof StatusTable.prototype.id,
  StatusTableRelations
> {

  public readonly synopsisCalls: HasManyRepositoryFactory<SynopsisCall, typeof StatusTable.prototype.id>;

  constructor(@inject('datasources.db') dataSource: PmssDataSource, @repository.getter('SynopsisCallRepository') protected synopsisCallRepositoryGetter: Getter<SynopsisCallRepository>,) {
    super(StatusTable, dataSource);
    this.synopsisCalls = this.createHasManyRepositoryFactoryFor('synopsisCalls', synopsisCallRepositoryGetter,);
    this.registerInclusionResolver('synopsisCalls', this.synopsisCalls.inclusionResolver);
  }
}

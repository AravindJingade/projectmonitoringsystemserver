import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {PmssDataSource} from '../datasources';
import {Group, Student, StudentGroup, StudentRelations} from '../models';
import {GroupRepository} from './group.repository';
import {StudentGroupRepository} from './student-group.repository';

export class StudentRepository extends DefaultCrudRepository<
  Student,
  typeof Student.prototype.id,
  StudentRelations
> {

  public readonly group: BelongsToAccessor<Group, typeof Student.prototype.id>;

  public readonly studentGroups: HasManyRepositoryFactory<StudentGroup, typeof Student.prototype.id>;

  constructor(@inject('datasources.db') dataSource: PmssDataSource, @repository.getter('GroupRepository') protected groupRepositoryGetter: Getter<GroupRepository>, @repository.getter('StudentGroupRepository') protected studentGroupRepositoryGetter: Getter<StudentGroupRepository>,) {
    super(Student, dataSource);
    this.studentGroups = this.createHasManyRepositoryFactoryFor('studentGroups', studentGroupRepositoryGetter,);
    this.registerInclusionResolver('studentGroups', this.studentGroups.inclusionResolver);
  }
}

import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, repository} from '@loopback/repository';
import {PmssDataSource} from '../datasources';
import {Group, Student, StudentGroup, StudentGroupRelations} from '../models';
import {GroupRepository} from './group.repository';
import {StudentRepository} from './student.repository';

export class StudentGroupRepository extends DefaultCrudRepository<
  StudentGroup,
  typeof StudentGroup.prototype.id,
  StudentGroupRelations
> {

  public readonly student: BelongsToAccessor<Student, typeof StudentGroup.prototype.id>;

  public readonly group: BelongsToAccessor<Group, typeof StudentGroup.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: PmssDataSource, @repository.getter('StudentRepository') protected studentRepositoryGetter: Getter<StudentRepository>, @repository.getter('GroupRepository') protected groupRepositoryGetter: Getter<GroupRepository>,
  ) {
    super(StudentGroup, dataSource);
    this.group = this.createBelongsToAccessorFor('group', groupRepositoryGetter,);
    this.registerInclusionResolver('group', this.group.inclusionResolver);
    this.student = this.createBelongsToAccessorFor('student', studentRepositoryGetter,);
    this.registerInclusionResolver('student', this.student.inclusionResolver);
  }
}

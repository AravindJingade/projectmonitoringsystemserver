import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {PmssDataSource} from '../datasources';
import {Department, DepartmentRelations, Faculty} from '../models';
import {FacultyRepository} from './faculty.repository';

export class DepartmentRepository extends DefaultCrudRepository<
  Department,
  typeof Department.prototype.id,
  DepartmentRelations
> {

  public readonly faculties: HasManyRepositoryFactory<Faculty, typeof Department.prototype.id>;

  constructor(@inject('datasources.db') dataSource: PmssDataSource, @repository.getter('FacultyRepository') protected facultyRepositoryGetter: Getter<FacultyRepository>,) {
    super(Department, dataSource);
    this.faculties = this.createHasManyRepositoryFactoryFor('faculties', facultyRepositoryGetter,);
    this.registerInclusionResolver('faculties', this.faculties.inclusionResolver);
  }
}

import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory} from '@loopback/repository';
import {PmssDataSource} from '../datasources';
import {Faculty, FacultyRelations, Department, Group} from '../models';
import {DepartmentRepository} from './department.repository';
import {GroupRepository} from './group.repository';

export class FacultyRepository extends DefaultCrudRepository<
  Faculty,
  typeof Faculty.prototype.id,
  FacultyRelations
> {

  public readonly dept: BelongsToAccessor<Department, typeof Faculty.prototype.id>;

  public readonly groups: HasManyRepositoryFactory<Group, typeof Faculty.prototype.id>;

  constructor(@inject('datasources.db') dataSource: PmssDataSource, @repository.getter('DepartmentRepository') protected departmentRepositoryGetter: Getter<DepartmentRepository>, @repository.getter('GroupRepository') protected groupRepositoryGetter: Getter<GroupRepository>,) {
    super(Faculty, dataSource);
    this.groups = this.createHasManyRepositoryFactoryFor('groups', groupRepositoryGetter,);
    this.registerInclusionResolver('groups', this.groups.inclusionResolver);
    this.dept = this.createBelongsToAccessorFor('dept', departmentRepositoryGetter);
    this.registerInclusionResolver('dept', this.dept.inclusionResolver);
  }
}

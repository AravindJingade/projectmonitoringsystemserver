import {Getter, inject} from '@loopback/core';
import {BelongsToAccessor, DefaultCrudRepository, HasManyRepositoryFactory, repository} from '@loopback/repository';
import {PmssDataSource} from '../datasources';
import {Faculty, Group, GroupRelations, Student, StudentGroup, SynopsisCall} from '../models';
import {FacultyRepository} from './faculty.repository';
import {StudentGroupRepository} from './student-group.repository';
import {StudentRepository} from './student.repository';
import {SynopsisCallRepository} from './synopsis-call.repository';

export class GroupRepository extends DefaultCrudRepository<
  Group,
  typeof Group.prototype.id,
  GroupRelations
> {

  public readonly faculty: BelongsToAccessor<Faculty, typeof Group.prototype.id>;

  public readonly students: HasManyRepositoryFactory<Student, typeof Group.prototype.id>;

  public readonly studentGroups: HasManyRepositoryFactory<StudentGroup, typeof Group.prototype.id>;

  public readonly group: BelongsToAccessor<SynopsisCall, typeof Group.prototype.id>;

  public readonly synopsisCalls: HasManyRepositoryFactory<SynopsisCall, typeof Group.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: PmssDataSource, @repository.getter('FacultyRepository') protected facultyRepositoryGetter: Getter<FacultyRepository>, @repository.getter('StudentRepository') protected studentRepositoryGetter: Getter<StudentRepository>, @repository.getter('StudentGroupRepository') protected studentGroupRepositoryGetter: Getter<StudentGroupRepository>, @repository.getter('SynopsisCallRepository') protected synopsisCallRepositoryGetter: Getter<SynopsisCallRepository>,
  ) {
    super(Group, dataSource);
    this.synopsisCalls = this.createHasManyRepositoryFactoryFor('synopsisCalls', synopsisCallRepositoryGetter,);
    this.registerInclusionResolver('synopsisCalls', this.synopsisCalls.inclusionResolver);
    this.studentGroups = this.createHasManyRepositoryFactoryFor('studentGroups', studentGroupRepositoryGetter,);
    this.registerInclusionResolver('studentGroups', this.studentGroups.inclusionResolver);
    this.faculty = this.createBelongsToAccessorFor('faculty', facultyRepositoryGetter,);
    this.registerInclusionResolver('faculty', this.faculty.inclusionResolver);
  }
}

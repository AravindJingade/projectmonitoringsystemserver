import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody
} from '@loopback/rest';
import {Faculty} from '../models';
import {FacultyRepository} from '../repositories';
import {HelperserviceService} from '../services';

export class FacultyController {
  constructor(
    @repository(FacultyRepository)
    public facultyRepository: FacultyRepository,
    @inject('services.helperserviceService')
    public helperserviceService: HelperserviceService,
  ) { }

  @post('/faculties', {
    responses: {
      '200': {
        description: 'Faculty model instance',
        content: {'application/json': {schema: getModelSchemaRef(Faculty)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Faculty, {
            title: 'NewFaculty',
            exclude: ['id'],
          }),
        },
      },
    })
    faculty: Omit<Faculty, 'id'>,
  ): Promise<Faculty> {
    return this.facultyRepository.create(faculty);
  }

  @get('/faculties/count', {
    responses: {
      '200': {
        description: 'Faculty model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Faculty) where?: Where<Faculty>): Promise<Count> {
    return this.facultyRepository.count(where);
  }

  @get('/faculties', {
    responses: {
      '200': {
        description: 'Array of Faculty model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Faculty, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Faculty) filter?: Filter<Faculty>,
  ): Promise<Faculty[]> {
    // return await this.helperserviceService.findFaulty(filter);
    return this.facultyRepository.find(filter);
  }

  @patch('/faculties', {
    responses: {
      '200': {
        description: 'Faculty PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Faculty, {partial: true}),
        },
      },
    })
    faculty: Faculty,
    @param.where(Faculty) where?: Where<Faculty>,
  ): Promise<Count> {
    return this.facultyRepository.updateAll(faculty, where);
  }

  @get('/faculties/{id}', {
    responses: {
      '200': {
        description: 'Faculty model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Faculty, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Faculty, {exclude: 'where'})
    filter?: FilterExcludingWhere<Faculty>,
  ) {
    // return await this.helperserviceService.findFaulty(id);
    return this.facultyRepository.findById(id, filter);
  }

  @patch('/faculties/{id}', {
    responses: {
      '204': {
        description: 'Faculty PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Faculty, {partial: true}),
        },
      },
    })
    faculty: Faculty,
  ): Promise<void> {
    await this.facultyRepository.updateById(id, faculty);
  }

  @put('/faculties/{id}', {
    responses: {
      '204': {
        description: 'Faculty PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() faculty: Faculty,
  ): Promise<void> {
    await this.facultyRepository.replaceById(id, faculty);
  }

  @del('/faculties/{id}', {
    responses: {
      '204': {
        description: 'Faculty DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.facultyRepository.deleteById(id);
  }
}

import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  StudentGroup,
  Student,
} from '../models';
import {StudentGroupRepository} from '../repositories';

export class StudentGroupStudentController {
  constructor(
    @repository(StudentGroupRepository)
    public studentGroupRepository: StudentGroupRepository,
  ) { }

  @get('/student-groups/{id}/student', {
    responses: {
      '200': {
        description: 'Student belonging to StudentGroup',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Student)},
          },
        },
      },
    },
  })
  async getStudent(
    @param.path.number('id') id: typeof StudentGroup.prototype.id,
  ): Promise<Student> {
    return this.studentGroupRepository.student(id);
  }
}

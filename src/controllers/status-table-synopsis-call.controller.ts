import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  StatusTable,
  SynopsisCall,
} from '../models';
import {StatusTableRepository} from '../repositories';

export class StatusTableSynopsisCallController {
  constructor(
    @repository(StatusTableRepository) protected statusTableRepository: StatusTableRepository,
  ) { }

  @get('/status-tables/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'Array of StatusTable has many SynopsisCall',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(SynopsisCall)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<SynopsisCall>,
  ): Promise<SynopsisCall[]> {
    return this.statusTableRepository.synopsisCalls(id).find(filter);
  }

  @post('/status-tables/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'StatusTable model instance',
        content: {'application/json': {schema: getModelSchemaRef(SynopsisCall)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof StatusTable.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SynopsisCall, {
            title: 'NewSynopsisCallInStatusTable',
            exclude: ['id'],
            optional: ['id']
          }),
        },
      },
    }) synopsisCall: Omit<SynopsisCall, 'id'>,
  ): Promise<SynopsisCall> {
    return this.statusTableRepository.synopsisCalls(id).create(synopsisCall);
  }

  @patch('/status-tables/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'StatusTable.SynopsisCall PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SynopsisCall, {partial: true}),
        },
      },
    })
    synopsisCall: Partial<SynopsisCall>,
    @param.query.object('where', getWhereSchemaFor(SynopsisCall)) where?: Where<SynopsisCall>,
  ): Promise<Count> {
    return this.statusTableRepository.synopsisCalls(id).patch(synopsisCall, where);
  }

  @del('/status-tables/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'StatusTable.SynopsisCall DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(SynopsisCall)) where?: Where<SynopsisCall>,
  ): Promise<Count> {
    return this.statusTableRepository.synopsisCalls(id).delete(where);
  }
}

import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Faculty,
  Department,
} from '../models';
import {FacultyRepository} from '../repositories';

export class FacultyDepartmentController {
  constructor(
    @repository(FacultyRepository)
    public facultyRepository: FacultyRepository,
  ) { }

  @get('/faculties/{id}/department', {
    responses: {
      '200': {
        description: 'Department belonging to Faculty',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Department)},
          },
        },
      },
    },
  })
  async getDepartment(
    @param.path.number('id') id: typeof Faculty.prototype.id,
  ): Promise<Department> {
    return this.facultyRepository.dept(id);
  }
}

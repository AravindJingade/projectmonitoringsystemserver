import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  SynopsisCall,
  Group,
} from '../models';
import {SynopsisCallRepository} from '../repositories';

export class SynopsisCallGroupController {
  constructor(
    @repository(SynopsisCallRepository)
    public synopsisCallRepository: SynopsisCallRepository,
  ) { }

  @get('/synopsis-calls/{id}/group', {
    responses: {
      '200': {
        description: 'Group belonging to SynopsisCall',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Group)},
          },
        },
      },
    },
  })
  async getGroup(
    @param.path.number('id') id: typeof SynopsisCall.prototype.id,
  ): Promise<Group> {
    return this.synopsisCallRepository.group(id);
  }
}

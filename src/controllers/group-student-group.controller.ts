import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Group,
  StudentGroup,
} from '../models';
import {GroupRepository} from '../repositories';

export class GroupStudentGroupController {
  constructor(
    @repository(GroupRepository) protected groupRepository: GroupRepository,
  ) { }

  @get('/groups/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Array of Group has many StudentGroup',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(StudentGroup)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<StudentGroup>,
  ): Promise<StudentGroup[]> {
    return this.groupRepository.studentGroups(id).find(filter);
  }

  @post('/groups/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Group model instance',
        content: {'application/json': {schema: getModelSchemaRef(StudentGroup)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Group.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {
            title: 'NewStudentGroupInGroup',
            exclude: ['id'],
            optional: ['id']
          }),
        },
      },
    }) studentGroup: Omit<StudentGroup, 'id'>,
  ): Promise<StudentGroup> {
    return this.groupRepository.studentGroups(id).create(studentGroup);
  }

  @patch('/groups/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Group.StudentGroup PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {partial: true}),
        },
      },
    })
    studentGroup: Partial<StudentGroup>,
    @param.query.object('where', getWhereSchemaFor(StudentGroup)) where?: Where<StudentGroup>,
  ): Promise<Count> {
    return this.groupRepository.studentGroups(id).patch(studentGroup, where);
  }

  @del('/groups/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Group.StudentGroup DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(StudentGroup)) where?: Where<StudentGroup>,
  ): Promise<Count> {
    return this.groupRepository.studentGroups(id).delete(where);
  }
}

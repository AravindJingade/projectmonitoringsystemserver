import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {StatusTable} from '../models';
import {StatusTableRepository} from '../repositories';

export class StatusController {
  constructor(
    @repository(StatusTableRepository)
    public statusTableRepository : StatusTableRepository,
  ) {}

  @post('/status-tables', {
    responses: {
      '200': {
        description: 'StatusTable model instance',
        content: {'application/json': {schema: getModelSchemaRef(StatusTable)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StatusTable, {
            title: 'NewStatusTable',
            exclude: ['id'],
          }),
        },
      },
    })
    statusTable: Omit<StatusTable, 'id'>,
  ): Promise<StatusTable> {
    return this.statusTableRepository.create(statusTable);
  }

  @get('/status-tables/count', {
    responses: {
      '200': {
        description: 'StatusTable model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(StatusTable) where?: Where<StatusTable>,
  ): Promise<Count> {
    return this.statusTableRepository.count(where);
  }

  @get('/status-tables', {
    responses: {
      '200': {
        description: 'Array of StatusTable model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(StatusTable, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(StatusTable) filter?: Filter<StatusTable>,
  ): Promise<StatusTable[]> {
    return this.statusTableRepository.find(filter);
  }

  @patch('/status-tables', {
    responses: {
      '200': {
        description: 'StatusTable PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StatusTable, {partial: true}),
        },
      },
    })
    statusTable: StatusTable,
    @param.where(StatusTable) where?: Where<StatusTable>,
  ): Promise<Count> {
    return this.statusTableRepository.updateAll(statusTable, where);
  }

  @get('/status-tables/{id}', {
    responses: {
      '200': {
        description: 'StatusTable model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(StatusTable, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StatusTable, {exclude: 'where'}) filter?: FilterExcludingWhere<StatusTable>
  ): Promise<StatusTable> {
    return this.statusTableRepository.findById(id, filter);
  }

  @patch('/status-tables/{id}', {
    responses: {
      '204': {
        description: 'StatusTable PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StatusTable, {partial: true}),
        },
      },
    })
    statusTable: StatusTable,
  ): Promise<void> {
    await this.statusTableRepository.updateById(id, statusTable);
  }

  @put('/status-tables/{id}', {
    responses: {
      '204': {
        description: 'StatusTable PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() statusTable: StatusTable,
  ): Promise<void> {
    await this.statusTableRepository.replaceById(id, statusTable);
  }

  @del('/status-tables/{id}', {
    responses: {
      '204': {
        description: 'StatusTable DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.statusTableRepository.deleteById(id);
  }
}

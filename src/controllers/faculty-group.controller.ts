import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Faculty,
  Group,
} from '../models';
import {FacultyRepository} from '../repositories';

export class FacultyGroupController {
  constructor(
    @repository(FacultyRepository) protected facultyRepository: FacultyRepository,
  ) { }

  @get('/faculties/{id}/groups', {
    responses: {
      '200': {
        description: 'Array of Faculty has many Group',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Group)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Group>,
  ): Promise<Group[]> {
    return this.facultyRepository.groups(id).find(filter);
  }

  @post('/faculties/{id}/groups', {
    responses: {
      '200': {
        description: 'Faculty model instance',
        content: {'application/json': {schema: getModelSchemaRef(Group)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Faculty.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Group, {
            title: 'NewGroupInFaculty',
            exclude: ['id'],
            optional: ['facultyId']
          }),
        },
      },
    }) group: Omit<Group, 'id'>,
  ): Promise<Group> {
    return this.facultyRepository.groups(id).create(group);
  }

  @patch('/faculties/{id}/groups', {
    responses: {
      '200': {
        description: 'Faculty.Group PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Group, {partial: true}),
        },
      },
    })
    group: Partial<Group>,
    @param.query.object('where', getWhereSchemaFor(Group)) where?: Where<Group>,
  ): Promise<Count> {
    return this.facultyRepository.groups(id).patch(group, where);
  }

  @del('/faculties/{id}/groups', {
    responses: {
      '200': {
        description: 'Faculty.Group DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Group)) where?: Where<Group>,
  ): Promise<Count> {
    return this.facultyRepository.groups(id).delete(where);
  }
}

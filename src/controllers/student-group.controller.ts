import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, put, requestBody,
  response
} from '@loopback/rest';
import {StudentGroup} from '../models';
import {StudentGroupRepository} from '../repositories';

export class StudentGroupController {
  constructor(
    @repository(StudentGroupRepository)
    public studentGroupRepository: StudentGroupRepository,
  ) { }

  @post('/stu-group')
  @response(200, {
    description: 'StudentGroup model instance',
    content: {'application/json': {schema: getModelSchemaRef(StudentGroup)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {
            title: 'NewStudentGroup',
            exclude: ['id'],
          }),
        },
      },
    })
    studentGroup: Omit<StudentGroup, 'id'>,
  ): Promise<StudentGroup> {
    return this.studentGroupRepository.create(studentGroup);
  }


  @post('/stu-group-bulk')
  @response(200, {
    description: 'StudentGroup model instance',
    content: {'application/json': {schema: getModelSchemaRef(StudentGroup)}},
  })
  async createBulk(
    @requestBody.array({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {
            title: 'NewStudentGroup',
            exclude: ['id'],
          }),
        },
      },
    })
    studentGroup: Omit<StudentGroup, 'id'>[],
  ): Promise<StudentGroup[]> {
    return this.studentGroupRepository.createAll(studentGroup);
  }

  @get('/stu-group/count')
  @response(200, {
    description: 'StudentGroup model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StudentGroup) where?: Where<StudentGroup>,
  ): Promise<Count> {
    return this.studentGroupRepository.count(where);
  }

  @get('/stu-group')
  @response(200, {
    description: 'Array of StudentGroup model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StudentGroup, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StudentGroup) filter?: Filter<StudentGroup>,
  ): Promise<StudentGroup[]> {
    return this.studentGroupRepository.find(filter);
  }

  @patch('/stu-group')
  @response(200, {
    description: 'StudentGroup PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {partial: true}),
        },
      },
    })
    studentGroup: StudentGroup,
    @param.where(StudentGroup) where?: Where<StudentGroup>,
  ): Promise<Count> {
    return this.studentGroupRepository.updateAll(studentGroup, where);
  }

  @get('/stu-group/{id}')
  @response(200, {
    description: 'StudentGroup model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StudentGroup, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StudentGroup, {exclude: 'where'}) filter?: FilterExcludingWhere<StudentGroup>
  ): Promise<StudentGroup> {
    return this.studentGroupRepository.findById(id, filter);
  }

  @patch('/stu-group/{id}')
  @response(204, {
    description: 'StudentGroup PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {partial: true}),
        },
      },
    })
    studentGroup: StudentGroup,
  ): Promise<void> {
    await this.studentGroupRepository.updateById(id, studentGroup);
  }

  @put('/stu-group/{id}')
  @response(204, {
    description: 'StudentGroup PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() studentGroup: StudentGroup,
  ): Promise<void> {
    await this.studentGroupRepository.replaceById(id, studentGroup);
  }

  @del('/stu-group/{id}')
  @response(204, {
    description: 'StudentGroup DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.studentGroupRepository.deleteById(id);
  }
}

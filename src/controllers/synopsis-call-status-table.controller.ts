import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  SynopsisCall,
  StatusTable,
} from '../models';
import {SynopsisCallRepository} from '../repositories';

export class SynopsisCallStatusTableController {
  constructor(
    @repository(SynopsisCallRepository)
    public synopsisCallRepository: SynopsisCallRepository,
  ) { }

  @get('/synopsis-calls/{id}/status-table', {
    responses: {
      '200': {
        description: 'StatusTable belonging to SynopsisCall',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(StatusTable)},
          },
        },
      },
    },
  })
  async getStatusTable(
    @param.path.number('id') id: typeof SynopsisCall.prototype.id,
  ): Promise<StatusTable> {
    return this.synopsisCallRepository.statusInfo(id);
  }
}

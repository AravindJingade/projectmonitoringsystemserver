import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Group,
  Faculty,
} from '../models';
import {GroupRepository} from '../repositories';

export class GroupFacultyController {
  constructor(
    @repository(GroupRepository)
    public groupRepository: GroupRepository,
  ) { }

  @get('/groups/{id}/faculty', {
    responses: {
      '200': {
        description: 'Faculty belonging to Group',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Faculty)},
          },
        },
      },
    },
  })
  async getFaculty(
    @param.path.number('id') id: typeof Group.prototype.id,
  ): Promise<Faculty> {
    return this.groupRepository.faculty(id);
  }
}

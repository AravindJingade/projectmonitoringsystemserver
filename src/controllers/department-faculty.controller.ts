import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {Department, Faculty} from '../models';
import {DepartmentRepository} from '../repositories';

export class DepartmentFacultyController {
  constructor(
    @repository(DepartmentRepository)
    protected departmentRepository: DepartmentRepository,
  ) {}

  @get('/departments/{id}/faculties', {
    responses: {
      '200': {
        description: 'Array of Department has many Faculty',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Faculty)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Faculty>,
  ): Promise<Faculty[]> {

    return this.departmentRepository.faculties(id).find(filter);
  }

  @post('/departments/{id}/faculties', {
    responses: {
      '200': {
        description: 'Department model instance',
        content: {'application/json': {schema: getModelSchemaRef(Faculty)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Department.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Faculty, {
            title: 'NewFacultyInDepartment',
            exclude: ['id', 'deptId'],
          }),
        },
      },
    })
    faculty: Omit<Faculty, 'id'>,
  ): Promise<Faculty> {
    faculty.deptId = id;
    // console.log('gggggggggggggggggggggggggg', faculty);

    return this.departmentRepository.faculties(id).create(faculty);
  }

  @patch('/departments/{id}/faculties', {
    responses: {
      '200': {
        description: 'Department.Faculty PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Faculty, {partial: true}),
        },
      },
    })
    faculty: Partial<Faculty>,
    @param.query.object('where', getWhereSchemaFor(Faculty))
    where?: Where<Faculty>,
  ): Promise<Count> {
    return this.departmentRepository.faculties(id).patch(faculty, where);
  }

  @del('/departments/{id}/faculties', {
    responses: {
      '200': {
        description: 'Department.Faculty DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Faculty))
    where?: Where<Faculty>,
  ): Promise<Count> {
    return this.departmentRepository.faculties(id).delete(where);
  }
}

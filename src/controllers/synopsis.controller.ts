import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, put, requestBody,
  RestBindings
} from '@loopback/rest';
import multer from 'multer';
import * as path from 'path';
import {SynopsisCall} from '../models';
import {SynopsisCallRepository} from '../repositories';

export class SynopsisController {
  constructor(
    @repository(SynopsisCallRepository)
    public synopsisCallRepository: SynopsisCallRepository,
  ) { }

  @post('/synopsis-calls', {
    responses: {
      '200': {
        description: 'SynopsisCall model instance',
        content: {'application/json': {schema: getModelSchemaRef(SynopsisCall)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SynopsisCall, {
            title: 'NewSynopsisCall',
            exclude: ['id'],
          }),
        },
      },
    })
    synopsisCall: Omit<SynopsisCall, 'id'>,
  ): Promise<SynopsisCall> {
    return this.synopsisCallRepository.create(synopsisCall);
  }


  @post('/upload-files', {
    responses: {
      '200': {
        description: 'AdvanceStatstics model instance',
      },
    },
  })
  async uploadFile(
    @requestBody({
      description: 'multipart/form-data value.',
      required: true,
      content: {
        'multipart/form-data': {
          'x-parser': 'stream',
          schema: {type: 'object'},
        },
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              actionDetails: {type: 'object'},
            },
          },
        },
      },
    })
    request: any,
    @inject(RestBindings.Http.RESPONSE) response: any,
  ) {
    console.log(request);

    let data: any = await this.uploadFiles(request, response);
    console.log('dataaaa', data);

    let myFile = data.files[0].originalname.split('.');
    const fileType = myFile[myFile.length - 1];
    console.log('filetType', fileType);

    data['results'] = {
      msg: 'FileUploaded Sucessfully',
      statusCode: 200,
    };
    return data;
  }

  async uploadFiles(request: any, response: any): Promise<object> {
    console.log(request);

    const storage = multer.diskStorage({
      destination: function (request, file, cb) {
        cb(null, 'uploads/');
      },
      filename: function (request, file, cb) {
        cb(
          null,
          path.basename(file.originalname) +
          '-' +
          Date.now() +
          path.extname(file.originalname),
        );
      },
    });

    // const storage = multer.memoryStorage({
    //   destination: function (request: any, file: any, cb: any) {
    //     cb(null, '');
    //   },
    // });
    console.log("fdgdfgfd");

    const upload = multer({storage: storage});

    // const upload = multer({storage});
    return new Promise<object>((resolve, reject) => {
      upload.any()(request, response, (err: any) => {
        if (err) reject(err);
        else {
          console.log(request.files);

          resolve({
            files: request.files,
            fields: (request as any).fields,
          });
        }
      });
    });
  }

  @get('/synopsis-calls/count', {
    responses: {
      '200': {
        description: 'SynopsisCall model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(SynopsisCall) where?: Where<SynopsisCall>,
  ): Promise<Count> {
    return this.synopsisCallRepository.count(where);
  }

  @get('/synopsis-calls', {
    responses: {
      '200': {
        description: 'Array of SynopsisCall model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(SynopsisCall, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(SynopsisCall) filter?: Filter<SynopsisCall>,
  ): Promise<SynopsisCall[]> {
    return this.synopsisCallRepository.find(filter);
  }

  @patch('/synopsis-calls', {
    responses: {
      '200': {
        description: 'SynopsisCall PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SynopsisCall, {partial: true}),
        },
      },
    })
    synopsisCall: SynopsisCall,
    @param.where(SynopsisCall) where?: Where<SynopsisCall>,
  ): Promise<Count> {
    return this.synopsisCallRepository.updateAll(synopsisCall, where);
  }

  @get('/synopsis-calls/{id}', {
    responses: {
      '200': {
        description: 'SynopsisCall model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(SynopsisCall, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(SynopsisCall, {exclude: 'where'}) filter?: FilterExcludingWhere<SynopsisCall>
  ): Promise<SynopsisCall> {
    return this.synopsisCallRepository.findById(id, filter);
  }

  @patch('/synopsis-calls/{id}', {
    responses: {
      '204': {
        description: 'SynopsisCall PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SynopsisCall, {partial: true}),
        },
      },
    })
    synopsisCall: SynopsisCall,
  ): Promise<void> {
    await this.synopsisCallRepository.updateById(id, synopsisCall);
  }

  @put('/synopsis-calls/{id}', {
    responses: {
      '204': {
        description: 'SynopsisCall PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() synopsisCall: SynopsisCall,
  ): Promise<void> {
    await this.synopsisCallRepository.replaceById(id, synopsisCall);
  }

  @del('/synopsis-calls/{id}', {
    responses: {
      '204': {
        description: 'SynopsisCall DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.synopsisCallRepository.deleteById(id);
  }
}

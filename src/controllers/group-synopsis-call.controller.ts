import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Group,
  SynopsisCall,
} from '../models';
import {GroupRepository} from '../repositories';

export class GroupSynopsisCallController {
  constructor(
    @repository(GroupRepository) protected groupRepository: GroupRepository,
  ) { }

  @get('/groups/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'Array of Group has many SynopsisCall',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(SynopsisCall)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<SynopsisCall>,
  ): Promise<SynopsisCall[]> {
    return this.groupRepository.synopsisCalls(id).find(filter);
  }

  @post('/groups/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'Group model instance',
        content: {'application/json': {schema: getModelSchemaRef(SynopsisCall)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Group.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SynopsisCall, {
            title: 'NewSynopsisCallInGroup',
            exclude: ['id'],
            optional: ['groupId']
          }),
        },
      },
    }) synopsisCall: Omit<SynopsisCall, 'id'>,
  ): Promise<SynopsisCall> {
    return this.groupRepository.synopsisCalls(id).create(synopsisCall);
  }

  @patch('/groups/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'Group.SynopsisCall PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SynopsisCall, {partial: true}),
        },
      },
    })
    synopsisCall: Partial<SynopsisCall>,
    @param.query.object('where', getWhereSchemaFor(SynopsisCall)) where?: Where<SynopsisCall>,
  ): Promise<Count> {
    return this.groupRepository.synopsisCalls(id).patch(synopsisCall, where);
  }

  @del('/groups/{id}/synopsis-calls', {
    responses: {
      '200': {
        description: 'Group.SynopsisCall DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(SynopsisCall)) where?: Where<SynopsisCall>,
  ): Promise<Count> {
    return this.groupRepository.synopsisCalls(id).delete(where);
  }
}

import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Student,
  StudentGroup,
} from '../models';
import {StudentRepository} from '../repositories';

export class StudentStudentGroupController {
  constructor(
    @repository(StudentRepository) protected studentRepository: StudentRepository,
  ) { }

  @get('/students/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Array of Student has many StudentGroup',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(StudentGroup)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<StudentGroup>,
  ): Promise<StudentGroup[]> {
    return this.studentRepository.studentGroups(id).find(filter);
  }

  @post('/students/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Student model instance',
        content: {'application/json': {schema: getModelSchemaRef(StudentGroup)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Student.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {
            title: 'NewStudentGroupInStudent',
            exclude: ['id'],
            optional: ['id']
          }),
        },
      },
    }) studentGroup: Omit<StudentGroup, 'id'>,
  ): Promise<StudentGroup> {
    return this.studentRepository.studentGroups(id).create(studentGroup);
  }

  @patch('/students/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Student.StudentGroup PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentGroup, {partial: true}),
        },
      },
    })
    studentGroup: Partial<StudentGroup>,
    @param.query.object('where', getWhereSchemaFor(StudentGroup)) where?: Where<StudentGroup>,
  ): Promise<Count> {
    return this.studentRepository.studentGroups(id).patch(studentGroup, where);
  }

  @del('/students/{id}/student-groups', {
    responses: {
      '200': {
        description: 'Student.StudentGroup DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(StudentGroup)) where?: Where<StudentGroup>,
  ): Promise<Count> {
    return this.studentRepository.studentGroups(id).delete(where);
  }
}

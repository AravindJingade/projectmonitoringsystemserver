import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  StudentGroup,
  Group,
} from '../models';
import {StudentGroupRepository} from '../repositories';

export class StudentGroupGroupController {
  constructor(
    @repository(StudentGroupRepository)
    public studentGroupRepository: StudentGroupRepository,
  ) { }

  @get('/student-groups/{id}/group', {
    responses: {
      '200': {
        description: 'Group belonging to StudentGroup',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Group)},
          },
        },
      },
    },
  })
  async getGroup(
    @param.path.number('id') id: typeof StudentGroup.prototype.id,
  ): Promise<Group> {
    return this.studentGroupRepository.group(id);
  }
}
